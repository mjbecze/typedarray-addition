# SYNOPSIS 

[![NPM Package](https://img.shields.io/npm/v/typedarray-addition.svg?style=flat-square)](https://www.npmjs.org/package/typedarray-addition)
[![Build Status](https://img.shields.io/travis/wanderer/typedarray-addition.svg?branch=master&style=flat-square)](https://travis-ci.org/wanderer/typedarray-addition)
[![Coverage Status](https://img.shields.io/coveralls/wanderer/typedarray-addition.svg?style=flat-square)](https://coveralls.io/r/wanderer/typedarray-addition)

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)  

This adds TypedArrays of arbitrary length together

# INSTALL
`npm install typedarray-addition`

# USAGE

```javascript
const typedAddition = require('typedarray-addition')
const result = typedAddition.add(new Uint8Array([1, 2, 4]), new Uint8Array([42, 4, 1]))
console.log(result) // [43, 6, 5]
```

# API
## add

Adds two typed arrays together. The arrays must be the same bitwidth

**Parameters**

-   `a` **[TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)** 
-   `b` **[TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)** 

Returns **[TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)** the result is stored in a new typed array

## addn

Adds a typed arrays and a natural number together

**Parameters**

-   `a` **[TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)** 
-   `n` **[Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** 

Returns **[TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)** the result is stored in a new typed array

# LICENSE
[MPL-2.0](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2))
