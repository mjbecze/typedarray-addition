const tape = require('tape')
const typedAddition = require('../index.js')

tape('tests', (t) => {
  let r = typedAddition.addn(new Uint8Array([0]), 2)
  t.deepEqual(r, [2], 'should add single vale')

  r = typedAddition.add(new Uint8Array([1, 2, 4]), new Uint8Array([42, 4, 1]))
  t.deepEqual(r, [43, 6, 5], 'should add two typed arrays')

  r = typedAddition.add(new Uint8Array([1, 2, 4]), new Uint8Array([42, 4, 1, 8]))
  t.deepEqual(r, [43, 6, 5, 8], 'should add two typed arrays')

  r = typedAddition.add(new Uint8Array([1, 255, 4]), new Uint8Array([42, 4, 1]))
  t.deepEqual(r, [43, 3, 6], 'should carry overflows')

  r = typedAddition.add(new Uint8Array([255, 255, 255]), new Uint8Array([255, 255, 255]))
  t.deepEqual(r, [254, 255, 255, 1], 'should carry overflows')

  r = typedAddition.add(new Uint8Array([1, 255, 4, 0, 0]), new Uint8Array([42, 4, 1]))
  t.deepEqual(r, [43, 3, 6], 'should trim zeros')

  t.end()
})
