const assert = require('assert')

/**
 * Adds two typed arrays together. The arrays must be the same bitwidth
 * @param {TypedArray} a
 * @param {TypedArray} b
 * @return {TypedArray} the result is stored in a new typed array
 */
exports.add = (a, b) => {
  assert.strict.equal(a.BYTES_PER_ELEMENT, b.BYTES_PER_ELEMENT, 'both arrays must be of the same type')

  const [longest, shortest] = a.length < b.length ? [b, a] : [a, b]
  const result = new longest.constructor(longest.length + 1)
  let carry = 0

  for (let i = 0; i < longest.length; i++) {
    result[i] = shortest[i] + carry
    carry = 0

    if (result[i] < shortest[i]) {
      carry = 1
    }

    result[i] += longest[i]
    if (result[i] < shortest[i]) {
      carry = 1
    }
  }

  // add the last carry
  result[longest.length] += carry

  // trim the remaining zeros
  let lastZero = result.length
  for (let i = result.length - 1; i > 0; i--) {
    if (result[i] === 0) {
      lastZero = i
    } else {
      break
    }
  }

  return result.subarray(0, lastZero)
}

/**
 * Adds a typed arrays and a natural number together
 * @param {TypedArray} a
 * @param {Number} n
 * @return {TypedArray} the result is stored in a new typed array
 */
exports.addn = (a, n) => {
  const maxVal = Math.pow(2, a.BYTES_PER_ELEMENT * 8)
  assert.strict.equal(maxVal > n, true, `natural value must be less ${maxVal}`)
  return exports.add(a, new a.constructor([n]))
}
